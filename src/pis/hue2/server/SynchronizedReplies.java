package pis.hue2.server;

import java.io.*;

public class SynchronizedReplies {
    
    private File path;
    private String fileName;
    private byte[] fileNameBytes;
    private byte[] fileContentBytes;
    private FileInputStream fileIN;
    private FileOutputStream fileOUT;

    
    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "PUT" Request an dem Server sendet.
     */
    public synchronized void put(DataInputStream in, DataOutputStream out) {
        try {
            out.writeUTF("ACK");
            if (in.readUTF().equals("DAT")) {
                //receiving the length of the byte array of the filename
                int fileName_ByteArrayLength = in.readInt();
                if (fileName_ByteArrayLength>0) {
                    //receiving the byte array of the filename and saving it as string
                    this.fileNameBytes = new byte[fileName_ByteArrayLength];
                    in.readFully(this.fileNameBytes, 0, fileName_ByteArrayLength);
                    this.fileName = new String(this.fileNameBytes);
                    //initializing the path
                    this.path = new File("/home/alex/Desktop/Studium/5.Semester/PiS/HÜ2/src/pis/hue2/server/Dateien/"+this.fileName);
                    if (!this.path.exists()) {//checking if this path already exist

                        out.writeUTF("ACK");

                        this.path.createNewFile();

                        this.fileOUT = new FileOutputStream(this.path);
                        //receiving the length of byte array of the content of the file
                        int fileContent_ByteArrayLength = in.readInt();
                        if (fileContent_ByteArrayLength>0) {
                            //receiving the the content of the file in the byte array form and writing it to the file
                            this.fileContentBytes = new byte[fileContent_ByteArrayLength];
                            in.readFully(this.fileContentBytes, 0, fileContent_ByteArrayLength);
                            this.fileOUT.write(this.fileContentBytes);
                            this.fileOUT.close();
                            
                        }
                        out.writeUTF("ACK");
                    }else{
                        out.writeUTF("DND");
                    }
                }
                
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();;
        }
    }

    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "GET" Request an dem Server sendet.
     */
    public synchronized void get(DataInputStream in, DataOutputStream out) {
        try {
            this.fileName = in.readUTF();
            this.path = new File("/home/alex/Desktop/Studium/5.Semester/PiS/HÜ2/src/pis/hue2/server/Dateien/"+this.fileName);
            if (this.path.exists()) {
                out.writeUTF("ACK");
                if (in.readUTF().equals("ACK")) {
                    out.writeUTF("DAT");
                    if (in.readUTF().equals("ACK")) {

                        this.fileIN = new FileInputStream(path);
                        //getting the name of the file and saving it to byte array
                        this.fileName = this.path.getName();
                        this.fileNameBytes = this.fileName.getBytes();
                        //saving the content of the file to byte array
                        this.fileContentBytes = new byte[(int)this.path.length()];
                        this.fileIN.read(fileContentBytes);
                        this.fileIN.close();
                        //sending the length of byte array of the filename and then the actual array to the client
                        out.writeInt(this.fileNameBytes.length);
                        out.write(this.fileNameBytes);
                        //sending the length of byte array of the filecontent and then the actual array to the client
                        out.writeInt(this.fileContentBytes.length);
                        out.write(this.fileContentBytes);
                    }
                }
            }else {//if the file dosen't exist, then client will receive DND mesage
                out.writeUTF("DND");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();;
        }
    }

    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "DEL" Request an dem Server sendet.
     */
    public synchronized void del(DataInputStream in, DataOutputStream out) {
        try {
            this.fileName = in.readUTF();
            this.path = new File("/home/alex/Desktop/Studium/5.Semester/PiS/HÜ2/src/pis/hue2/server/Dateien/"+fileName);
            if (!this.path.exists()) {
                out.writeUTF("DND");
            }else if(this.path.delete()) {
                out.writeUTF("ACK");
            }else {
                out.writeUTF("DND");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();;
        }
    }
}
