package pis.hue2.server;

import java.io.IOException;
import java.net.*;

/**
 * Diese Klasse erstellt ein ServerSocket, mit dem sich mehrere Sockets verbinden können. 
 * Die Interaktion mit den Sockets wird in separaten Threads durchgeführt.
 * 
 * @see java.lang.Runnable
 * 
 * @author Alexandr Ergheledji
 * @version 2.0
 */

public class Server implements Runnable{

    private ServerSocket server;
    private boolean stopRequested = false;
    private SynchronizedReplies reply;

    /**
     * Konstrukter der Klasse Server initialisiert den ServerSocket.
     * 
     *  
     */

    public Server(){
        try {
            this.server = new ServerSocket(8080);
            System.out.println("Server is started");
            this.reply = new SynchronizedReplies();
        } catch (IOException e) {
            System.out.println("Cannot open port 8080");
            e.printStackTrace();
        }
    }


    /**
     * Diese Methode wartet auf neue Verbindungen und kommt dann zur Ende, wenn die Methode  isStopRequestedd() true zurück liefert.
     */
    @Override
    public void run() {
        while (!isStopRequested()) {
            try {
                Socket client = null;
                try {
                    client = server.accept();
                } catch (IOException ioE) {
                    if (isStopRequested()) {
                        System.out.println("Server is stopped");
                        return;
                    }
                    throw new RuntimeException("Error during client conection");
                }
                
                
                ClientHandler cHandler = new ClientHandler(client, this.reply);
    
                new Thread(cHandler).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Server is stopped");
    }

    /**
     * Diese Methode setzt den Wert der globalen Variable stopRequested auf true und 
     * schließt das ServerSocket.
     */
    public void requestStop() {
        this.stopRequested = true;
        try {
            this.server.close();
        } catch (IOException e) {
            throw new RuntimeException("Error during the closing");
        }
    }

    /**
     * Diese Mathode gibt den Wert der globalen booleschen Variablen "stopRequested" zurück.
     * 
     * @return den Wert der globalen Variable stopRequested
     */
    public boolean isStopRequested() {
        return this.stopRequested;
    }
     
}
