package pis.hue2.server;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Diese Klasse ist zuständig für die Kommunikation mit einem Client(Socket), den sie übergeben wird. 
 * 
 * @see java.lang.Runnable
 * 
 * @author Alexandr Ergheledji
 * @version 3.0
 */

public class ClientHandler implements Runnable {

    private Socket clientSocket;
    private static int counter = 0;
    private int id;
    private boolean stopRequested = false;
    private DataInputStream in;
    private DataOutputStream out;

    private File path;
    private String fileName;
    private byte[] fileNameBytes;
    private byte[] fileContentBytes;
    private FileInputStream fileIN;
    private FileOutputStream fileOUT;

    private SynchronizedReplies reply;
    

    /**
     * Konstrukter der Klasse ClientHandler 
     * 
     * @param clientSocket
     */
    public ClientHandler(Socket clientSocket, SynchronizedReplies reply) {
    
        try {
            this.clientSocket = clientSocket;
            counter += 1;
            this.id = ThreadLocalRandom.current().nextInt(10, 100);
            this.in = new DataInputStream(clientSocket.getInputStream());
            this.out = new DataOutputStream(clientSocket.getOutputStream());

            this.reply = reply;
        } catch (IOException e) {
            System.out.println("Error during conection");
            e.printStackTrace();
        }
        
    }

    /**
     * Diese Methode ampfängt die Requests von dem Socket und entscheidet entschprechend, 
     * wie ClientHandler mit dem Client kommunizieren soll.
     */
    @Override
    public void run() {
        String message = "";
        while (!isStopRequested()) {
            try {
                message = this.in.readUTF();
            } catch (IOException e) {
                System.out.println("Errror during receiving the message");
                e.printStackTrace();
            }
            /* if(!message.equals("CON")) */ System.out.println("CLIENT_"+this.id+": "+message);
            switch (message) {
                case "CON":
                    con();
                    break;
                case "DSC":
                    dsc();
                    break;
                case "LST":
                    lst();
                    break;
                case "PUT":
                    this.reply.put(this.in, this.out);
                    break;
                case "GET":
                    this.reply.get(this.in, this.out);
                    break;
                case "DEL":
                    this.reply.del(this.in, this.out);
                    break;
            }
            //if(message.equals("CON")) System.out.println("CLIENT_"+counter+": "+message);
        }
        
    }

    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "CON" Request an dem Server sendet.
     */
    public void con() {
        try {
            if (counter<4) {
                System.out.println("New client connected " + this.clientSocket.getInetAddress().getHostAddress());
                this.out.writeUTF("ACK");
            }else{
                System.out.println("CLIENT_"+this.id+": CON - unsuccessfully");
                this.out.writeUTF("DND");
                requestStop();
            }
        } catch (IOException e) {
            System.out.println("CLIENT_"+this.id+": CON - unsuccessfully");
            e.printStackTrace();
        }
    }

    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "DSC" Request an dem Server sendet.
     */
    public void dsc() {
        try {
            this.out.writeUTF("DSC");
            System.out.println("Client"+this.id+" is disconnected");
            requestStop();
        } catch (IOException e) {
            System.out.println("Client"+this.id+": DSC - unsuccessfully");
            e.printStackTrace();
        }
    }

    /**
     * Diese Methode ist ein Kommunikationsverfahren mit dem Client für den Fall, 
     * wenn der Client ein "LST" Request an dem Server sendet.
     */
    public void lst() {
        try {

            this.out.writeUTF("ACK");
            if (this.in.readUTF().equals("ACK")) {
                this.out.writeUTF("DAT");
                this.path = new File("/home/alex/Desktop/Studium/5.Semester/PiS/HÜ2/src/pis/hue2/server/Dateien");
                String[] paths = this.path.list();
                
                if (paths != null) {
                    //sending the number of elements in the list with names
                    this.out.writeInt(paths.length);
                    //sending the elements of the list
                    for (String string : paths) {
                        //sending the length of the byte array of name and then the byte array itself
                        this.out.writeInt(string.length());
                        this.out.write(string.getBytes());
                    }
                    if (!this.in.readUTF().equals("ACK")) {
                        System.out.println("Client"+this.id+": LST - successfully");
                    }
                    
                }else this.out.writeInt(0);
            }
        } catch (IOException e) {
            System.out.println("Client"+this.id+": LST - unsuccessfully");
            e.printStackTrace();
        } catch (NullPointerException ne) {
            ne.printStackTrace();;
        }
    }

    
    /**
     * Diese Methode setzt den Wert der globalen booleschen Variable auf true und 
     * schließt die Kommunikationswege mit dem Socket und das Socket.
     */
    public void requestStop() {
        this.stopRequested = true;
        try {
            this.in.close();
            this.out.close();
            this.clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        counter -= 1;
    }

    /**
     * Diese Mathode gibt den Wert der globalen booleschen Variablen "stopRequested" zurück.
     * 
     * @return den Wert der globalen Variable stopRequested
     */
    public boolean isStopRequested() {
        return this.stopRequested;
    }
}
