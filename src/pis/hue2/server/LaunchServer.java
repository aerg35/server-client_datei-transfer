package pis.hue2.server;

import java.util.Scanner;

public class LaunchServer {
    private static Scanner scann = new Scanner(System.in);
    public static void main(String[] args) {
        Server server = new Server();
        new Thread(server).start();

        while (true) {
            if (scann.next().equals("stop")) {
                server.requestStop();
                return;
            }
        }
        
    }
}
