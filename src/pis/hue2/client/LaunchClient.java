package pis.hue2.client;



/**
 * Klasse LaunchCLient startet die graphische Oberflaeche des Clients.
 * Jeder Client wird in einem neuen Prozess behandelt.
 * @author phili
 *
 */
public class LaunchClient {

    public static void main(String[] args) {
    	new Thread(new Runnable() {

			@Override
			public void run() {
				ClientGUI clientgui = new ClientGUI();				
			}			
		}).start(); 
    }
}
