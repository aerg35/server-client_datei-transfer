package pis.hue2.client;

import java.awt.*;
import java.io.IOException;


import javax.swing.*;
/**
 * Die Klasse "ClientGUI" ist die graphische Oberflaeche für einen Client.
 * Man kann Nachrichten eingeben und diese dann absenden. 
 * Außerdem werden die Nachrichten vom Server ausgegeben.
 * Zusaetzlich wird der Status eines Clients angezeigt, ob dieser On- oder Offline ist.
 * @author phili
 *
 */

public class ClientGUI extends JFrame {
	
	//private boolean wasStarted = false;
	private Client client;
    private boolean trigger = false;

    private JPanel l1_WestPanel;
    private JPanel l1_EastPanel;
    private JPanel l1_CenterPanel;
    
    private JPanel l2_InputPanel;
    private JPanel l2_OutputPanel;

    private JPanel l3_TextInputPanel;
    private JPanel l3_ButtonsInputPanel;

    private JPanel l3_ServerOutputPanel;
    private JPanel l3_StatusOutputPanel;
	

    private JLabel messageToServer;
    private JLabel serverMes;
    private JLabel aktiviert;
    private JButton sendeMsg;
    private JTextField konsole;

	public ClientGUI()
	{
        this.setLayout(new BorderLayout());

        this.l1_WestPanel = new JPanel();
        this.l1_WestPanel.setPreferredSize(new Dimension(20,20));
        this.add(this.l1_WestPanel, BorderLayout.WEST);
        this.l1_EastPanel = new JPanel();
        this.l1_EastPanel.setPreferredSize(new Dimension(20,20));
        this.add(this.l1_EastPanel, BorderLayout.EAST);
        this.l1_CenterPanel = new JPanel(new GridLayout(2,1));
        this.add(this.l1_CenterPanel, BorderLayout.CENTER);

        this.l2_InputPanel = new JPanel(new GridLayout(2,1));
        this.l1_CenterPanel.add(this.l2_InputPanel);
        this.l2_OutputPanel = new JPanel(new GridLayout(1,2));
        this.l1_CenterPanel.add(this.l2_OutputPanel);

        this.l3_TextInputPanel = new JPanel(new GridLayout(2,1));
        this.l2_InputPanel.add(this.l3_TextInputPanel);
        this.l3_ButtonsInputPanel = new JPanel();
        this.l2_InputPanel.add(this.l3_ButtonsInputPanel);

        this.l3_ServerOutputPanel = new JPanel();
        this.l2_OutputPanel.add(this.l3_ServerOutputPanel);
        this.l3_StatusOutputPanel = new JPanel();
        this.l2_OutputPanel.add(this.l3_StatusOutputPanel);
		
		this.setTitle("ClientGUI");  //Titel des Frames
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		
		
        this.setSize(new Dimension(500,450));
        this.setMinimumSize(new Dimension(500,400));
		

        this.serverMes = new JLabel();
        this.l3_ServerOutputPanel.add(this.serverMes);

        this.messageToServer = new JLabel("message:");
        this.l3_TextInputPanel.add(messageToServer);
		
		
		this.aktiviert = new JLabel("STATUS - offline");  //Label für das Anzeigen vom Status
        this.l3_StatusOutputPanel.add(this.aktiviert);

		
		
		
		this.sendeMsg = new JButton("send");  //Button für das Absenden einer Nachricht
		this.sendeMsg.setPreferredSize(new Dimension(200,25));
        this.l3_ButtonsInputPanel.add(this.sendeMsg, BorderLayout.EAST);
 
		
		
		this.konsole = new JTextField(SwingConstants.CENTER);  //Chat-Eingabe
        this.konsole.setPreferredSize(new Dimension(300,25));
        this.l3_TextInputPanel.add(this.konsole);

		
        this.konsole.addActionListener(e -> 
        {
            sendMessage();
        });
		
		this.sendeMsg.addActionListener(e ->
		{
            sendMessage();
		});
							
		this.pack();
		this.setVisible(true);
	}

	
	/**
	 * Die Methode "sendMessage" fuehrt die Methoden der "Client" Klasse aus und kuemmert sich dazu um die korrekte Darstellung in der GUI.
	 * 
	 * Die Eingabe in der Konsole wird in der "switch" behandelt und loest den entsprechenden Fall aus. 
	 * In jedem Fall wird die graphische Oberflaeche veraendert und die jeweilige Methode der Klasse "Client" ausgefuehrt.
	 * 
	 * Fall "CON":
	 *   Ein Objekt der Klasse "Client" wird erstellt sowie eine Verbindung zwischen Server und Client.
	 *   
	 * Fall "DCS": 
	 *   Ein Client beendet die Verbindung zum Server.
	 *   
	 * Fall "LST":
	 *   Eine Liste von Dateien im Besitz des Servers wird angefordert und ausgegeben.
	 *   
	 * Fall "PUT":
	 *   Eine Datei vom Client wird zum Dateiverzeichnis des Server hinzugefuegt.
	 *   
	 * Fall "GET":
	 *   Eine Datei vom Server wird zum Client geschickt.
	 *   
	 * Fall "DEL":
	 *   Loescht eine Datei aus dem Verzeichnis des Servers.
	 */
    
    private void sendMessage() {
        String[] message = konsole.getText().split(" ", 2);
            String[] list = null;
            String answer = "";
            if (trigger || message[0].toUpperCase().equals("CON")) {
                trigger = true;
                switch (message[0].toUpperCase()) {
                    case "CON": 
                        try {
                            this.client = new Client();
                            if(this.client.con().equals("ACK")) {
                                this.aktiviert.setText("STATUS - online");
                                answer = "SUCCESSFULLY";
                            }
                            else answer = "UNSUCCESSFULLY";
                            this.serverMes.setText("<html><B>SERVER:</B><br/>CON - "+answer+"</html>");
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>CON - ERROR</html>");
                            e1.printStackTrace();
                        }
                        
                        break;
                    case "DSC":
                        try {
                            if(this.client.dsc().equals("DSC")) {
                                this.aktiviert.setText("STATUS - offline");
                                answer = "SUCCESSFULLY";
                                trigger = false;
                                this.client.clientStop();
                            }
                            else answer = "UNSUCCESSFULLY";
    
                            this.serverMes.setText("<html><B>SERVER:</B><br/>DSC - "+answer+"</html>");
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>DSC - ERROR</html>");
                            e1.printStackTrace();
                        }
                        break;
                    case "LST":
                        try {
                            list = this.client.lst();
                            if(list.length > 0) {
                                answer ="LST<br/>";
                                for(int i = 0; i<list.length; i++) {
                                    answer = answer +"\t"+(i+1)+" - "+list[i]+"<br/>";
                                }
                                answer.replaceAll("\t", "       ");
    
                            }else answer = "LST <br/>there is no files";
    
                            this.serverMes.setText("<html><B>SERVER:</B><br/>"+answer+"</html>");
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>LST - ERROR</html>");
                            e1.printStackTrace();
                        }
                        break;
                    case "PUT":
                        try {
                            if(message.length>1) {
    
                                answer = this.client.put(message[1]);
                                if(answer.equals("ACK")) answer = "SUCCESSFULLY";
                                else answer = "UNSUCCESSFULLY<br/>"+answer;
                                this.serverMes.setText("<html><B>SERVER:</B><br/>PUT - "+answer+"</html>");
    
                            }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");                        
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>PUT - ERROR</html>");
                            e1.printStackTrace();
                        }
                        break;
                    case "GET":
                        try {
                            if(message.length>1) {
    
                                answer = this.client.get(message[1]);
                                if(answer.equals("ACK")) answer = "SUCCESSFULLY";
                                else answer = "UNSUCCESSFULLY<br/>"+answer;
                                this.serverMes.setText("<html><B>SERVER:</B><br/>GET - "+answer+"</html>");
    
                            }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>GET - ERROR</html>");
                            e1.printStackTrace();
                        }
                        break;
                    case "DEL":
                        try {
                            if(message.length>1) {
    
                                if(this.client.del(message[1]).equals("ACK")) answer = "SUCCESSFULLY";
                                else answer = "UNSUCCESSFULLY";
                                this.serverMes.setText("<html><B>SERVER:</B><br/>DEL - "+answer+"</html>");
    
                            }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");
                        } catch (IOException e1) {
                            this.serverMes.setText("<html><B>SERVER:</B><br/>DEL - ERROR</html>");
                            e1.printStackTrace();
                        }
                        break;
                    default:
                    	this.serverMes.setText("unknown command");
                }
            } else {
                this.serverMes.setText("You are not connected");
            }
            
            
            /* switch (message[0].toUpperCase()) {
                case "CON": 
                    try {
                        this.client = new Client();
                        if(this.client.con().equals("ACK")) {
                            this.aktiviert.setText("STATUS - online");
                            answer = "SUCCESSFULLY";
                        }
                        else answer = "UNSUCCESSFULLY";
                        this.serverMes.setText("<html><B>SERVER:</B><br/>CON - "+answer+"</html>");
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>CON - ERROR</html>");
                        e1.printStackTrace();
                    }
                    
                    break;
                case "DSC":
                    try {
                        if(this.client.dsc().equals("DSC")) {
                            this.aktiviert.setText("STATUS - offline");
                            answer = "SUCCESSFULLY";
                        }
                        else answer = "UNSUCCESSFULLY";

                        this.serverMes.setText("<html><B>SERVER:</B><br/>DSC - "+answer+"</html>");
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>DSC - ERROR</html>");
                        e1.printStackTrace();
                    }
                    break;
                case "LST":
                    try {
                        list = this.client.lst();
                        if(list.length > 0) {
                            answer ="LST<br/>";
                            for(int i = 0; i<list.length; i++) {
                                answer = answer +"\t"+(i+1)+" - "+list[i]+"<br/>";
                            }
                            answer.replaceAll("\t", "       ");

                        }else answer = "LST <br/>there is no files";

                        this.serverMes.setText("<html><B>SERVER:</B><br/>"+answer+"</html>");
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>LST - ERROR</html>");
                        e1.printStackTrace();
                    }
                    break;
                case "PUT":
                    try {
                        if(message.length>1) {

                            answer = this.client.put(message[1]);
                            if(answer.equals("ACK")) answer = "SUCCESSFULLY";
                            else answer = "UNSUCCESSFULLY<br/>"+answer;
                            this.serverMes.setText("<html><B>SERVER:</B><br/>PUT - "+answer+"</html>");

                        }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");                        
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>PUT - ERROR</html>");
                        e1.printStackTrace();
                    }
                    break;
                case "GET":
                    try {
                        if(message.length>1) {

                            answer = this.client.get(message[1]);
                            if(answer.equals("ACK")) answer = "SUCCESSFULLY";
                            else answer = "UNSUCCESSFULLY<br/>"+answer;
                            this.serverMes.setText("<html><B>SERVER:</B><br/>GET - "+answer+"</html>");

                        }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>GET - ERROR</html>");
                        e1.printStackTrace();
                    }
                    break;
                case "DEL":
                    try {
                        if(message.length>1) {

                            if(this.client.del(message[1]).equals("ACK")) answer = "SUCCESSFULLY";
                            else answer = "UNSUCCESSFULLY";
                            this.serverMes.setText("<html><B>SERVER:</B><br/>DEL - "+answer+"</html>");

                        }else this.serverMes.setText("<html><B>ERROR:</B><br/> You have forgot to enter<br/> the name of the file</html>");
                    } catch (IOException e1) {
                        this.serverMes.setText("<html><B>SERVER:</B><br/>DEL - ERROR</html>");
                        e1.printStackTrace();
                    }
                    break;
            } */
    }
	
	
}


