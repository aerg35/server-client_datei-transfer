package pis.hue2.client;

import java.io.*;
import java.net.Socket;
/**
 * Die Klasse "Client" kann einen Client erstellen, der mittels TCP/IP-Verbindung über Sockets mit einem Server kommunizieren kann.
 * Ein Client kann Nachrichten senden und empfangen.
 * CON, DSC, LST, GET, PUT, DEL und DAT sind die Befehle des Protokolls.
 * @author phili
 *
 */
public class Client {

    private DataInputStream in;
    private DataOutputStream out;
    private Socket client;

    private File path;
    private String fileName;
    private byte[] fileNameBytes;
    private byte[] fileContentBytes;
    private FileInputStream fileIN;
    private FileOutputStream fileOUT;
/**
 * Die Socket eines Clients horcht auf dem Port 8080.
 * Jedes Objekt der Klasse Client erhält einen Input- und Outputstream.
 * @throws IOException
 */
    public Client() throws IOException {
        this.client = new Socket("localhost", 8080);
        this.in = new DataInputStream(this.client.getInputStream());
        this.out = new DataOutputStream(this.client.getOutputStream());
    }

/**
 * Methode "con"(Connect) schickt ein "CON" und gibt die Antwort des Servers aus.
 * @return  Antwort des Servers(ACK oder DND).
 * @throws IOException
 */
    public String con() throws IOException {
        this.out.writeUTF("CON");
        return this.in.readUTF();
    }
/**
 * Methode "dsc"(Disconnect) schickt ein DSC und gibt die Antwort des Servers aus.
 * @return Antwort des Servers(DSC oder DND).
 * @throws IOException
 */
    public String dsc() throws IOException {
        this.out.writeUTF("DSC");
        return this.in.readUTF();
    }
/**
 * Methode "lst"(List) schickt ein lst. Wenn ein ACK folgt, wird ein ACK zurueck zum Server geschickt. 
 * Danach sollte ein DAT vom Server kommen und eine Liste von Dateien geschickt werden.
 * Zum Ende schickt der Client ein ACK an den Server, um den erfolgreichen Ablauf des Prozesses zu bestaetigen.
 * @return eine Liste von Dateien.
 * @throws IOException
 */
    public String[] lst() throws IOException {
        String[] list = null;
        this.out.writeUTF("LST");
        if (this.in.readUTF().equals("ACK")) {
            this.out.writeUTF("ACK");
            if (this.in.readUTF().equals("DAT")) {
                list = new String[this.in.readInt()];
                byte[] bytes;
                
                if (list.length > 0) {
                    for (int i = 0; i < list.length; i++) {
                        bytes = new byte[this.in.readInt()];
                        this.in.readFully(bytes, 0, bytes.length);
                        list[i] = new String(bytes);
                    }
                    this.out.writeUTF("ACK");
                    
                }
            } 
        }
        return list;
    }
/**
 * Die Methode "put"(Put) fuegt eine Datei zum Dateiverzeichnis des Servers hinzu.
 * 
 * @param fName ist der Name der Datei.
 * @return String der in der GUI angezeigt wird,
 * @throws IOException
 */
    public String put(String fName) throws IOException {
        path = new File(fName);
        this.out.writeUTF("PUT");
        if (this.in.readUTF().equals("ACK")) {
            this.out.writeUTF("DAT");

            if (path.exists()) {

                fileIN = new FileInputStream(path);
                
                fileName = fName;
                fileNameBytes = fileName.getBytes();
                
                fileContentBytes = new byte[(int)path.length()];
                fileIN.read(fileContentBytes);
                fileIN.close();
                
                this.out.writeInt(fileNameBytes.length);
                this.out.write(fileNameBytes);
                
                if (this.in.readUTF().equals("ACK")) {
                    this.out.writeInt(fileContentBytes.length);
                    this.out.write(fileContentBytes);

                    if (this.in.readUTF().equals("ACK")) {

                        return "ACK";
                    } else {

                        return "DND";
                    }
                }else return "there is already such a file";
                
                
            }else return "You don't have file with such a name";
        }else return "server doesn't respond";
    } 
/**
 * Die Methode "get"(Get) laed eine Datei aus dem Verzeichnis vom Server in das Verzeichnis des Clients.
 * @param fName Name der Datei
 * @return String der in der GUI angezeigt wird.
 * @throws IOException
 */
    public String get(String fName) throws IOException {
        this.out.writeUTF("GET");
        this.out.writeUTF(fName);
        if (this.in.readUTF().equals("ACK")) {
            this.out.writeUTF("ACK");
            if (this.in.readUTF().equals("DAT")) {
                this.out.writeUTF("ACK");
                int fileName_ByteArrayLength = this.in.readInt();
                if (fileName_ByteArrayLength>0) {

                    fileNameBytes = new byte[fileName_ByteArrayLength];
                    this.in.readFully(fileNameBytes, 0, fileName_ByteArrayLength);
                    fileName = new String(fileNameBytes);

                    path = new File(fileName);

                    int fileContent_ByteArrayLength = this.in.readInt();
                    if (fileContent_ByteArrayLength>=0) {

                        fileContentBytes = new byte[fileContent_ByteArrayLength];
                        this.in.readFully(fileContentBytes, 0, fileContent_ByteArrayLength);

                        if (!path.exists()) {

                            path.createNewFile();
                            fileOUT = new FileOutputStream(path);

                            fileOUT.write(fileContentBytes);
                            fileOUT.close();
                            this.out.writeUTF("ACK");
                            return "ACK";
                            
                        }return "You have already such a file";
                        
                    }
                    
                }
            }
        }
        return "something went wrong";
    } 
/**
 * Die Methode "del"(Delete) loescht eine Datei aus dem Verzeichnis des Servers.
 * @param fName Name der Datei
 * @return Antwort des Servers.
 * @throws IOException
 */
    public String del(String fName) throws IOException {
        this.out.writeUTF("DEL");
        this.out.writeUTF(fName);
        return this.in.readUTF();
    } 
/**
 * Die Methode "clientStop" stoppt den Client.
 * Die Socket wird geschlossen.
 * 
 */
    public void clientStop(){
        try {
            this.in.close();
            this.out.close();
            this.client.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }    
}

